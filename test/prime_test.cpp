/**
 * Copyright (C) 2020  Gaurav Mishra
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <json/json.h>

#include "../prime.hpp"

using namespace std;

class primeTest : public CPPUNIT_NS ::TestFixture
{
    CPPUNIT_TEST_SUITE(primeTest);
    CPPUNIT_TEST(isPrime);
    CPPUNIT_TEST(isntPrime);

    CPPUNIT_TEST_SUITE_END();

protected:
    void isPrime(void)
    {
        Json::Value prime1;
        prime1["number"] = 11;

        Json::Value prime2;
        prime2["number"] = 743;

        PrimeDetector p;
        Json::Value actualResult1 = p.isPrime(prime1);
        Json::Value actualResult2 = p.isPrime(prime2);

        CPPUNIT_ASSERT_EQUAL(true, actualResult1["isPrime"].asBool());
        CPPUNIT_ASSERT_EQUAL(true, actualResult2["isPrime"].asBool());
        CPPUNIT_ASSERT_EQUAL(11, actualResult1["number"].asInt());
        CPPUNIT_ASSERT_EQUAL(743, actualResult2["number"].asInt());
    };

    void isntPrime(void)
    {
        Json::Value nonPrime1;
        nonPrime1["number"] = 121;

        Json::Value nonPrime2;
        nonPrime2["number"] = 747;

        PrimeDetector p;
        Json::Value actualResult1 = p.isPrime(nonPrime1);
        Json::Value actualResult2 = p.isPrime(nonPrime2);

        CPPUNIT_ASSERT_EQUAL(false, actualResult1["isPrime"].asBool());
        CPPUNIT_ASSERT_EQUAL(false, actualResult2["isPrime"].asBool());
        CPPUNIT_ASSERT_EQUAL(121, actualResult1["number"].asInt());
        CPPUNIT_ASSERT_EQUAL(747, actualResult2["number"].asInt());
    };
};

CPPUNIT_TEST_SUITE_REGISTRATION(primeTest);
